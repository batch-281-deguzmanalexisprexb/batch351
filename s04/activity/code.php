<?php 
// 1. Create multiple classes to demonstrate Access Modifiers and Encapsulation.
//  - Reference that will help you for accomplishing the activity:
//  https://www.phptutorial.net/php-oop/php-call-parent-constructor/


// 2. Create a "Person" class with the following properties, this properties must not be accessible outside the class but can be inherited by other classes.
//  - name
//  - age
//  - address

// 3. Implement a getter and setter methods for each properties of "Person" class.


class Person
{
	protected $name;
	protected $age;
	protected $address;

	function __construct($name, $age, $address)
	{
		$this->name = $name;
		$this->age = $age;
		$this->address = $address;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getAge()
	{
		return $this->age;
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function setAge($age)
	{
		$this->age = $age;
	}
	
	public function setAddress($address)
	{
		$this->address = $address;
	}
	
}

// 4. Create a "Student" class that inherits the "Person" class.
// 5. Add a protected property to the Student class for "studentId".
// 6. Implement a getter and setter method for the studentId property.

class Student extends Person
{
	protected $studentId;

	function __construct($name, $age, $address, $studentId)
	{
		parent::__construct($name, $age, $address);
		$this->studentId = $studentId;
	}

	public function getStudentId()
	{
		return $this->studentId;
	}

	public function setStudentId($studentId)
	{
		$this->studentId = $studentId;
	}
}

// 7. Create an "Employee" class that inherits the "Person" class.
// 8. Add a protected property to the Employee class for "team" and "role".
// 9. Implement a getter and setter method for the "team" and "role" property.

class Employee extends Person
{
	protected $team;
	protected $role;

	function __construct($name, $age, $address, $team, $role)
	{
		parent::__construct($name, $age, $address);
		$this->team = $team;
		$this->role = $role;
	}

	public function getTeam()
	{
		return $this->team;
	}

	public function getRole()
	{
		return $this->role;
	}

	public function setTeam($team)
	{
		$this->team = $team;
	}

	public function setRole($role)
	{
		$this->role = $role;
	}
}

$person = new Person("John Smith", 30, "Quezon City, Metro Manila");
$student = new Student("Jane Doe", 20, "Makati City, Metro Manila", "2023-1980");
$employee = new Employee("Mark Blain", 35, "Pasig City, Metro Manila", "Tech Team", "Team Lead");

// 10. Create instances of the Person, Student, and Job classes.
// 11. Display each instances information in the browser.