<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05 Activity</title>
</head>
<body>
	<?php session_start(); ?>
	<?php if (!isset($_SESSION['email'])): ?>
	<form method="POST" action="./server.php" style="display: inline-block;">
		<input type="hidden" name="action" value="login" />

		Email:
		<input type="text" name="email" required>

		Password:
		<input type="password" name="password" required>
		<button type="submit">Login</button>
	</form>
	<?php endif; ?>

	<?php if (isset($_SESSION['email'])): ?>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="logout" />
			Hello, <?= $_SESSION['email'] ?>
			<br>
			<br>
			<button type="submit">Logout</button>
		</form>
	<?php endif; ?>

	<br><br>

	<?php if (isset($_SESSION['login_error_message'])): ?>
		<?php echo $_SESSION['login_error_message']; ?>
	<?php endif; ?>
</body>
</html>