<?php require_once "./code.php"; ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 Activity</title>
	</head>
	<body>
		<h1>Divisible by Five</h1>
		<p><?php printDivisibleOfFive(); ?></p>

		<?php 
		// 5. In index.php, perform the following using array functions:
		// - Accept a name of the student and add it to the "students" array.
		// - Print the names added so far in the "students" array.
		// - Count the number of names in the "students" array.
		// - Add another student then print the array and its new count.
		// - Finally, remove the firs student and print the array and its count. 
		?>
		<?php array_push($students, 'John Smith'); ?>
		<p><?php print_r($students); ?></p>
		<p><?= count($students); ?></p>
		<?php array_push($students, 'Jane Smith'); ?>
		<p><?php print_r($students); ?></p>
		<p><?= count($students); ?></p>
		<?php array_shift($students); ?>
		<p><?php print_r($students); ?></p>
		<p><?= count($students); ?></p>

	</body>

</html>