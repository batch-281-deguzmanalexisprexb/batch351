<?php 

// 1. Create code.php and index.php inside the activity folder.

// 2. In code.php, create a function named printDivisibleOfFive that will perform the following:
// - Using loops, print all numbers that are divisible by 5 from 0 to 1000.
// - Stop the loop when the loop reaches its 100th iteration.
// function 

function printDivisibleOfFive() {
	for($number = 0; $number <= 100; $number++) {
		if($number % 5 === 0) {
			echo "$number ";
		}
	}
}

// 3. Invoke the printDivisibleOfFive() function in the index.php

// 4. In code.php again, create an empty array named "students".

$students = [];



?>
